# My string SSO

## Descriptions

This string is written in c++ and implemented using the SSO algorithm.  That is, this algorithm implements this way: if the string is too small, it is written to the memory stack, otherwise new memory will be allocated on the heap, or if the memory is already allocated on the stack, but the string is increased, then it simply goes again to the first allocation already on the heap.

## Comipilation

```
git clone https://gitlab.com/m9858/my-string-sso.git
make run or make; ./MyString
```

this will be a test code, but you can see by example how it works.
