#pragma once
#include <cstddef>
#include <ostream>

class string{
	private:
	    size_t _strSize;
	    bool flagHeap;
	    union{
		struct {
			char* arr;
			size_t Size;
		}strHeap;
		char strStack[sizeof(strHeap)];
	    };

	public:
	    string();//++
	    string(const char* cstr);//++
	    ~string();//++
	    string(const string& other);//++
	    string(string&& other) noexcept;//++

	    string& operator=(const string& other);//++
	    string& operator=(string&& other) noexcept;//++

	    bool operator==(const string& other) const;//++
	    char& operator[](size_t index);//++

	    string operator+(const string& other);//++
	    string& operator+=(const string& right);//++

	    void append(const char* str, size_t size);//++
	    void push_back(char c);//++

	    void erase(size_t pos);//++
	    char pop_back();//++
	    size_t size() const;//++
	    friend std::ostream& operator<<(std::ostream& out, const string& other);//++
     	    friend std::istream& operator>>(std::istream& in, string& other);//++
};


std::ostream& operator<<(std::ostream& out, const string& other);//++
std::istream& operator>>(std::istream& in, string& other);//++

