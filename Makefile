.PHONY: all clear run

all: main

main: test.o string.o
	g++ $^ -o MyString -g

%.o: %.c
	g++ -g -c $< -o $*.o 

clean:
	rm -f *.o MyString

run:main 
	./MyString
