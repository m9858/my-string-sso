#include "string.h"
#include <cassert>
#include <iostream>

string::string() :
      _strSize(0), flagHeap(false){      
}

string::string(const char* cstr){
	this->flagHeap = false;;
	const char* tmpCstr = cstr;
	while (*tmpCstr++);
	this->_strSize = tmpCstr - cstr - 1;
	if (this->_strSize > sizeof(this->strHeap)){
		flagHeap = true;
		this->strHeap.arr = new char[this->_strSize + 1];
		this->strHeap.Size = this->_strSize + 1;
	}
	char* tmp = this->flagHeap?  this->strHeap.arr : this->strStack;
	while (*tmp++ = *cstr++);
}


string::~string(){
	if (this->flagHeap){
		delete[] this->strHeap.arr;
		this->strHeap.Size = 0;
	}
	this->flagHeap = false;
}

string::string(const string& other){
	this->_strSize = other._strSize;
	this->flagHeap = other.flagHeap; 
	if (this->flagHeap){
		this->strHeap.Size = other.strHeap.Size;
		this->strHeap.arr = new char[this->strHeap.Size];
	}
	for (size_t i = 0; i < this->_strSize; i++){
		if (this->flagHeap){
			this->strHeap.arr[i] = other.strHeap.arr[i];
		}
		else{
			this->strStack[i] = other.strStack[i];
		}
	}
}

string::string(string&& other) noexcept{
	this->_strSize = other._strSize;
	this->flagHeap = other.flagHeap;
       	if (this->flagHeap){
		this->strHeap.arr = other.strHeap.arr;
		this->strHeap.Size = other.strHeap.Size;
		other.strHeap.arr = nullptr;
	}
	else{
		for (size_t i = 0; i < this->_strSize; i++){
			this->strStack[i] = other.strStack[i];
		}
	}
}


string& string::operator=(const string& other){
	this->~string();
	this->_strSize = other._strSize;
	this->flagHeap = other.flagHeap;
	if (this->flagHeap){
		this->strHeap.arr = new char[other.strHeap.Size]; 
		this->strHeap.Size = other.strHeap.Size;
	}
	const char* tmpOther = other.flagHeap? other.strHeap.arr : other.strStack;
	char* tmpThis = this->flagHeap? this->strHeap.arr : this->strStack;
	while(*tmpThis++ = *tmpOther++);
	return *this; 
}

string& string::operator=(string&& other) noexcept{
	if (this == &other){
		return *this;
	}
	if (this->flagHeap){
		delete[] this->strHeap.arr;
	}
	this->flagHeap = other.flagHeap;
	this->_strSize = other._strSize;
	if (this->flagHeap){
		this->strHeap.arr = other.strHeap.arr;
		this->strHeap.Size = other.strHeap.Size;
		other.strHeap.Size = 0;
		other.strHeap.arr = nullptr;
	}
	else{
		for (size_t i = 0; i <= other._strSize; i++){
			this->strStack[i] = other.strStack[i];
			other.strStack[i] = 0;
		}
	}
	other._strSize = 0;
	return *this;
}

string string::operator+(const string& other){
	string tmpStr;
	tmpStr.flagHeap = false;
	tmpStr._strSize = this->_strSize + other._strSize;
	if (sizeof(strHeap) < tmpStr._strSize){
		tmpStr.flagHeap = true;
		tmpStr.strHeap.arr = new char[tmpStr._strSize + 1];
		tmpStr.strHeap.Size = tmpStr._strSize + 1;
	}
	for (size_t i = 0; i < this->_strSize; i++){
		if (tmpStr.flagHeap){
			tmpStr.strHeap.arr[i] = !this->flagHeap? this->strStack[i] : this->strHeap.arr[i];
		}
		else{
			tmpStr.strStack[i] = !this->flagHeap? this->strStack[i] : this->strHeap.arr[i];
		}
	}
	for (size_t i = 0; i <= other._strSize; i++){
		if (tmpStr.flagHeap){
			tmpStr.strHeap.arr[this->_strSize + i] = other.flagHeap? other.strHeap.arr[i] : other.strStack[i];
		}
		else{
			tmpStr.strStack[this->_strSize + i] = other.flagHeap? other.strHeap.arr[i] : other.strStack[i];
		}
	}
	return tmpStr;
}


string& string::operator+=(const string& right){
	return *this = *this + right;
}

bool string::operator==(const string& other)const {
	const char* tmpOther = other.flagHeap ? other.strHeap.arr : other.strStack;
	const char* tmpThis = this->flagHeap ? this->strHeap.arr : this->strStack;
	while (*tmpThis == *tmpOther && *tmpOther && *tmpThis){
		tmpThis++;
		tmpOther++;
	}
	return !(*tmpThis - *tmpOther);
}


char& string::operator[](size_t index){
	assert(!(index >= this->_strSize));
	return this->flagHeap? this->strHeap.arr[index] : this->strStack[index];
}


void string::push_back(char c){
	if (!this->flagHeap && this->_strSize + 1 < sizeof(this->strHeap)){
		this->strStack[this->_strSize++] = c;
		this->strStack[this->_strSize] = 0;
	}
	else if (this->flagHeap && this->_strSize + 1 < this->strHeap.Size){
		this->strHeap.arr[this->_strSize++] = c;
		this->strHeap.arr[this->_strSize] = 0;
	}
	else{
		char *tmp = new char[this->_strSize + 2];
		for (size_t i = 0; i < _strSize; i++){
			tmp[i] = flagHeap? this->strHeap.arr[i] : this->strStack[i];
		}
		if (flagHeap){
			delete[] this->strHeap.arr;
		}
		flagHeap = true;
		this->strHeap.Size = this->_strSize + 2;
		this->strHeap.arr = tmp;
		tmp = nullptr;
		this->strHeap.arr[this->_strSize++]  = c;
		this->strHeap.arr[this->_strSize] = 0;
	}
}


void string::append(const char* str, size_t size){
	*this += string (str);
}



void string::erase(size_t pos){
	if (pos >= this->_strSize){
		return;
	}
	for (size_t i = pos; i < this->_strSize - 1; i++){
		if(this->flagHeap){
			this->strHeap.arr[i] = this->strHeap.arr[i + 1];
		}
		else{
			this->strStack[i] = this->strStack[i + 1];
		}
	}
	this->_strSize--;
	if(this->flagHeap){
		this->strHeap.arr[this->_strSize] = 0;
	}
	else{
		this->strStack[this->_strSize] = 0; 
	}

}


char string::pop_back(){
	assert(!this->_strSize == 0);
	char sumbol;
	if (this->flagHeap){
		sumbol = this->strHeap.arr[this->_strSize - 1]; 
		this->strHeap.arr[this->_strSize - 1] = 0;
	}
	else{
		sumbol = this->strStack[this->_strSize - 1];
		this->strStack[this->_strSize - 1] = 0;
	}
	this->_strSize--;
	return sumbol;
}


size_t string::size() const{
	return this->_strSize;
}


std::ostream& operator<<(std::ostream& out, const string& other){
	if (other.flagHeap){
		out <<	other.strHeap.arr;
	} 
	else{ 
		out << other.strStack;
	}
	return out;
}


std::istream& operator>>(std::istream& in, string& other){
	char* strIn = new char[100];
	size_t len = 0, highlighted = 100;
	char sumbol;
	while(true){
		sumbol = in.get();
		if (len + 1 ==  highlighted){
			char* tmp = strIn;
			strIn = nullptr;
			strIn = new char[highlighted += 100];
			for (size_t i = 0; i < len; i++){
				strIn[i] = tmp[i];
			}
			delete[] tmp;
		}
		if (sumbol != '\n' && sumbol != '\t' && sumbol != ' '){
			strIn[len++] = sumbol;
		}
		else{
			strIn[len++] = 0;
			break;
		}
	}
	if (other.flagHeap){
		delete[] other.strHeap.arr;
		other.flagHeap = false;
	}
	if (len < sizeof(other.strHeap)){
		for (size_t i = 0; i < len; i++){
			other.strStack[i] = strIn[i];
		}
	}
	else{
		other.flagHeap = true;
		other.strHeap.arr = new char[other.strHeap.Size = len];
		for (size_t i = 0; i < len; i++){
			other.strHeap.arr[i] = strIn[i];
		}
	}
	other._strSize = len - 1;
	delete[] strIn;
	return in;
}






